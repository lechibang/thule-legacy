package data.missions.testmissionthule;

import com.fs.starfarer.api.fleet.FleetGoal;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import com.fs.starfarer.api.mission.MissionDefinitionPlugin;

public class MissionDefinition implements MissionDefinitionPlugin {

	public void defineMission(MissionDefinitionAPI api) {

		// Set up the fleets
		api.initFleet(FleetSide.PLAYER, "TLS", FleetGoal.ATTACK, false);
		api.initFleet(FleetSide.ENEMY, "ISS", FleetGoal.ATTACK, true);

		// Set a blurb for each fleet
		api.setFleetTagline(FleetSide.PLAYER, "A memorable one-liner describing the player's fleet");
		api.setFleetTagline(FleetSide.ENEMY, "Another one liner, for the enemy fleet");
		
		// These show up as items in the bulleted list under 
		// "Tactical Objectives" on the mission detail screen
		api.addBriefingItem("These are hints to the player");
		api.addBriefingItem("As to how to approach the mission");
		api.addBriefingItem("Don't add more than 4 - they won't fit on the mission detail screen");
		
		// Set up the player's fleet
		api.addToFleet(FleetSide.PLAYER, "thule_vikingmki_HC", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "thule_vikingmki_HC", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "thule_vikingmki_HC", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "thule_vikingmki_HC", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "thule_vikingmkii_HC", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "thule_oberon_HC", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "thule_forsser_HC", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "thule_ragnarok_OD", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "thule_ragnarok_HC", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "thule_berserker_OD", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "thule_berserker_HC", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "thule_berserker_HC", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "thule_berserker_HC", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "thule_berserker_HC", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "thule_berghast_HC", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "thule_herzog_HC", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "thule_herzog_OD", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "thule_hansa_HC", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "thule_monarch_HC", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "thule_solidstorm_HC", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "thule_tungsten_Experimental", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.PLAYER, "thule_gunnr_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.PLAYER, "thule_komet_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.PLAYER, "thule_einherjer_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.PLAYER, "thule_einherjer_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.PLAYER, "thule_einherjer_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.PLAYER, "thule_einherjer_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.PLAYER, "thule_einherjer_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.PLAYER, "thule_einherjer_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.PLAYER, "thule_einherjer_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.PLAYER, "thule_einherjer_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.PLAYER, "thule_einherjer_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.PLAYER, "thule_einherjer_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.PLAYER, "thule_wartool_wing", FleetMemberType.FIGHTER_WING, false);
		
		
		
		// Mark a ship as essential, if you want
		//api.defeatOnShipLoss("ISS Black Star");
		
		// Set up the enemy fleet
		api.addToFleet(FleetSide.ENEMY, "onslaught_Standard", FleetMemberType.SHIP, false);
//		api.addToFleet(FleetSide.ENEMY, "onslaught_Outdated", FleetMemberType.SHIP, false);
//		api.addToFleet(FleetSide.ENEMY, "onslaught_Outdated", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "dominator_Assault", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "dominator_Assault", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "dominator_Support", FleetMemberType.SHIP, false);
		//api.addToFleet(FleetSide.ENEMY, "dominator_Support", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "condor_Strike", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "condor_Strike", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "enforcer_Assault", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "enforcer_Assault", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "enforcer_CS", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "enforcer_CS", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "hound_Assault", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "hound_Assault", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "lasher_CS", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "lasher_CS", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "lasher_CS", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "lasher_CS", FleetMemberType.SHIP, false);
		
		api.addToFleet(FleetSide.ENEMY, "talon_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.ENEMY, "talon_wing", FleetMemberType.FIGHTER_WING, false);
		//api.addToFleet(FleetSide.ENEMY, "talon_wing", FleetMemberType.FIGHTER_WING, false);
		//api.addToFleet(FleetSide.ENEMY, "talon_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.ENEMY, "broadsword_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.ENEMY, "broadsword_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.ENEMY, "broadsword_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.ENEMY, "broadsword_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.ENEMY, "piranha_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.ENEMY, "piranha_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.ENEMY, "piranha_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.ENEMY, "piranha_wing", FleetMemberType.FIGHTER_WING, false);
		
		
		// Set up the map.
		float width = 20000f;
		float height = 12000f;
		api.initMap((float)-width/2f, (float)width/2f, (float)-height/2f, (float)height/2f);
		
		float minX = -width/2;
		float minY = -height/2;
		
		for (int i = 0; i < 15; i++) {
			float x = (float) Math.random() * width - width/2;
			float y = (float) Math.random() * height - height/2;
			float radius = 100f + (float) Math.random() * 900f; 
			api.addNebula(x, y, radius);
		}
		
		api.addNebula(minX + width * 0.8f, minY + height * 0.4f, 2000);
		api.addNebula(minX + width * 0.8f, minY + height * 0.5f, 2000);
		api.addNebula(minX + width * 0.8f, minY + height * 0.6f, 2000);
		
		api.addObjective(minX + width * 0.8f, minY + height * 0.4f, "nav_buoy");
		api.addObjective(minX + width * 0.8f, minY + height * 0.6f, "nav_buoy");
		api.addObjective(minX + width * 0.3f, minY + height * 0.3f, "comm_relay");
		api.addObjective(minX + width * 0.3f, minY + height * 0.7f, "comm_relay");
		api.addObjective(minX + width * 0.5f, minY + height * 0.5f, "sensor_array");
		api.addObjective(minX + width * 0.2f, minY + height * 0.5f, "sensor_array");
		
		// Add an asteroid field
		 api.addAsteroidField(minX + width/2f, minY + height/2f, 0, 12000f,
								20f, 70f, 100);
		
		// Add some planets.  These are defined in data/config/planets.json.
		api.addPlanet(minX + width * 0.2f + 600, minY + height * 0.5f, 250f, "ice_giant", 200f);
		api.addPlanet(minX + width * 0.6f + 600, minY + height * 0.3f, 150f, "cryovolcanic", 100f);
	}

}



