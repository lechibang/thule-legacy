package data.shipsystems.scripts;

import com.fs.starfarer.api.Global;
import java.util.List;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import org.lwjgl.util.vector.Vector2f;
import java.util.Random;


/**
 *
 * @author Ender
 */
public class thule_bifrost_transmitter implements ShipSystemStatsScript {

    private ShipAPI shipsapi;
    private ShipAPI final_shipsapi;
    private CombatEntityAPI combatEntity;
    private CombatEntityAPI currentShip;
    private CombatEngineAPI engine;
    private MutableShipStatsAPI eachShip;
    private boolean hasRun = false;
   
    
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        engine = Global.getCombatEngine();
        
        if (engine != null && !hasRun) {            
            if (engine.getShips() != null) {
                        
                        Random randomNum = new Random();
                        List ships = engine.getShips();  //get a list of all active ships
                        List shipList = null;
                        currentShip = stats.getEntity(); //we need our current ship so that we know who owns it
                        Vector2f location = currentShip.getLocation();
                        for (int i = 0; i < ships.size(); i++) { //now we loop through the entire list of ships
                            shipsapi = (ShipAPI) (ships.get(i)); //assign the current ship to the ShipAPI so we can give it bonuses or penalties
                            eachShip = shipsapi.getMutableStats(); //now we need something Mutable
                            combatEntity = eachShip.getEntity(); //we need to assign it as an entity as well so we can see who owns it
                            
                            //in here, we're checking to find friendly ships
                            if ((combatEntity.getOwner() == currentShip.getOwner()) && (eachShip != null) && (combatEntity != stats.getEntity())) {
                                float randomX = (randomNum.nextInt(5) - 2) * 400;
                                float randomY = (randomNum.nextInt(5) - 2) * 400;
                                shipsapi.getLocation().x = (location.x + randomX);
                                shipsapi.getLocation().y = (location.y + randomY);
                            }
                        }
                        hasRun = true;
            }           
        }
    }

    
    public void unapply(MutableShipStatsAPI stats, String id) {
		//there's nothing to unapply here, so we can leave this block of code empty
                hasRun = false;
    }
    
    public StatusData getStatusData(int index, State state, float effectLevel) {
		if (index == 0) {
			return new StatusData("Teleporting fleet to near", false); //display status message to the player
		}
		return null;
	}
}
