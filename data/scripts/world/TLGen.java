package data.scripts.world;

import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.SectorGeneratorPlugin;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.shared.SharedData;
import data.scripts.world.systems.TLHeimdallSystem;

@SuppressWarnings("unchecked")
public class TLGen implements SectorGeneratorPlugin {

    @Override
    public void generate(SectorAPI sector) {
        new TLHeimdallSystem().generate(sector);
        
        SharedData.getData().getPersonBountyEventData().addParticipatingFaction("thulelegacy");

        FactionAPI thulelegacy = sector.getFaction("thulelegacy");

        FactionAPI player = sector.getFaction(Factions.PLAYER);
        FactionAPI hegemony = sector.getFaction(Factions.HEGEMONY);
        FactionAPI tritachyon = sector.getFaction(Factions.TRITACHYON);
        FactionAPI pirates = sector.getFaction(Factions.PIRATES);
        FactionAPI independent = sector.getFaction(Factions.INDEPENDENT);
        FactionAPI church = sector.getFaction(Factions.LUDDIC_CHURCH);
        FactionAPI path = sector.getFaction(Factions.LUDDIC_PATH);
        FactionAPI diktat = sector.getFaction(Factions.DIKTAT);
        FactionAPI persean = sector.getFaction(Factions.PERSEAN);

        thulelegacy.setRelationship(player.getId(), -1f);
        thulelegacy.setRelationship(hegemony.getId(), -1f);
        thulelegacy.setRelationship(tritachyon.getId(), -1f);
        thulelegacy.setRelationship(pirates.getId(), -1f);
        thulelegacy.setRelationship(independent.getId(), -1f);
        thulelegacy.setRelationship(church.getId(), -1f);
        thulelegacy.setRelationship(path.getId(), -1f);
        thulelegacy.setRelationship(diktat.getId(), -1f);
        thulelegacy.setRelationship(persean.getId(), -1f);
    }
}
