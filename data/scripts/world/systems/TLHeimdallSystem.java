package data.scripts.world.systems;

import com.fs.starfarer.api.campaign.PlanetAPI;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.SectorGeneratorPlugin;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.impl.campaign.ids.Conditions;
import com.fs.starfarer.api.impl.campaign.ids.StarTypes;
import com.fs.starfarer.api.impl.campaign.ids.Submarkets;
import com.fs.starfarer.api.impl.campaign.procgen.NebulaEditor;
import com.fs.starfarer.api.impl.campaign.terrain.HyperspaceTerrainPlugin;
import com.fs.starfarer.api.util.Misc;
import data.scripts.world.AddMarketplace;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;

public class TLHeimdallSystem implements SectorGeneratorPlugin {
    @Override
    public void generate(SectorAPI sector) {

        StarSystemAPI system = sector.createStarSystem("Heimdall");
        system.setBackgroundTextureFilename("graphics/backgrounds/background6.jpg");

        PlanetAPI star = system.initStar("heimdall", StarTypes.YELLOW, 900f, 600);
        system.setLightColor(new Color(255, 250, 250));

        system.getLocation().set(-1500, -9800);

        PlanetAPI slepnir = system.addPlanet("slepnir", star, "Slepnir", "toxic", 210, 250, 3500, 180);
        SectorEntityToken slepnirStation = system.addCustomEntity("slepnirStation", "Gungnir", "station_side07", "thulelegacy");
        slepnirStation.setCircularOrbitPointingDown(slepnir, 60, 420, 70);
        AddMarketplace.addMarketplace("thulelegacy",
                slepnir,
                new ArrayList<>(Arrays.asList(slepnirStation)),
                "Slepnir",
                3,
                new ArrayList<>(Arrays.asList(Conditions.MILITARY_BASE,
                        Conditions.SPACEPORT,
                        Conditions.ORBITAL_STATION,
                        Conditions.POPULATION_3,
                        Conditions.TOXIC_ATMOSPHERE)),
                new ArrayList<>(Arrays.asList(Submarkets.SUBMARKET_STORAGE,
                        Submarkets.SUBMARKET_BLACK,
                        Submarkets.SUBMARKET_OPEN,
                        Submarkets.GENERIC_MILITARY)),
                0.3f
        );
        
        PlanetAPI bifrost = system.addPlanet("bifrost", star, "Bifrost", "cryovolcanic", 300, 80, 7000, 365);
        system.addAsteroidBelt(bifrost, 20, 200, 100, 400, 600);
        bifrost.setFaction("thulelegacy");
        AddMarketplace.addMarketplace("thulelegacy",
                bifrost,
                null,
                "Bifrost",
                4,
                new ArrayList<>(Arrays.asList(Conditions.MILITARY_BASE,
                        Conditions.SPACEPORT,
                        Conditions.POPULATION_4,
                        Conditions.VOLATILES_PLENTIFUL,
                        Conditions.TECTONIC_ACTIVITY,
                        Conditions.ORE_RICH,
                        Conditions.ICE)),
                new ArrayList<>(Arrays.asList(Submarkets.SUBMARKET_STORAGE,
                        Submarkets.SUBMARKET_BLACK,
                        Submarkets.SUBMARKET_OPEN,
                        Submarkets.GENERIC_MILITARY)),
                0.3f
        );
        
        PlanetAPI bifrost2 = system.addPlanet("bifrost2", bifrost, "Bifrost II", "jungle", 30, 170, 800, 60);
        bifrost2.setFaction("thulelegacy");
        AddMarketplace.addMarketplace("thulelegacy",
                bifrost2,
                null,
                "Bifrost II",
                6,
                new ArrayList<>(Arrays.asList(Conditions.MILITARY_BASE,
                        Conditions.SPACEPORT,
                        Conditions.TRADE_CENTER,
                        Conditions.HEADQUARTERS,
                        Conditions.REGIONAL_CAPITAL,
                        Conditions.POPULATION_6,
                        Conditions.ORBITAL_STATION,
                        Conditions.URBANIZED_POLITY,
                        Conditions.JUNGLE)),
                new ArrayList<>(Arrays.asList(Submarkets.SUBMARKET_STORAGE,
                        Submarkets.SUBMARKET_BLACK,
                        Submarkets.SUBMARKET_OPEN,
                        Submarkets.GENERIC_MILITARY)),
                0.3f
        );

        system.autogenerateHyperspaceJumpPoints(true, true, true);

        cleanup(system);
    }

    void cleanup(StarSystemAPI system) {
        HyperspaceTerrainPlugin plugin = (HyperspaceTerrainPlugin) Misc.getHyperspaceTerrain().getPlugin();
        NebulaEditor editor = new NebulaEditor(plugin);
        float minRadius = plugin.getTileSize() * 2f;

        float radius = system.getMaxRadiusInHyperspace();
        editor.clearArc(system.getLocation().x, system.getLocation().y, 0, radius + minRadius * 0.5f, 0, 360f);
        editor.clearArc(system.getLocation().x, system.getLocation().y, 0, radius + minRadius, 0, 360f, 0.25f);
    }
}
